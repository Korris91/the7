﻿import Vue from 'vue'
import axios from 'axios'
import VeeValidate from 'vee-validate'
Vue.use(VeeValidate);
var app = new Vue({
    el: '#app',
    data: {
        usernameLogin: window.localStorage.getItem("usernameLogin"),
        passwordLogin: window.localStorage.getItem("passwordLogin"),
        usernameRegister: window.localStorage.getItem("usernameRegister"),
        passwordRegister: window.localStorage.getItem("passwordRegister"),
    },
    methods: {
        Display: function () {
            window.localStorage.setItem("usernameLogin", this.usernameLogin);
            window.localStorage.setItem("passwordLogin", this.passwordLogin);
            window.localStorage.setItem("usernameRegister", this.usernameRegister);
            window.localStorage.setItem("passwordRegister", this.passwordRegister);
        }
    }
});